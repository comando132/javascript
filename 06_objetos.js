function Persona(nombre) {
    this.nombre = nombre;
    console.log(this.nombre);
}

Persona.prototype.hola = function() {
    console.log('Hola ' + this.nombre);
};

Persona.prototype.adios = function() {
    console.log('Ya me voy, adios ' + this.nombre);
};

Persona.prototype.cambiarNombre = function(nombre) {
    this.nombre = nombre;
}


let persona1 = new Persona("Alicia");
let persona2 = new Persona("Eduardo");

// Llamadas al método diHola de la clase Persona.
persona1.hola(); // muestra "Hola, Soy Alicia"
persona1.cambiarNombre('Katia');
persona1.adios();

persona2.hola(); // muestra "Hola, Soy Eduardo"


