/* Ejercicio para el modulo de sentencias 
IF - ELSE, SWITCH
FOR, FOR - IN, FOR - OF
WHILE, DO - WHILE
*/
{
    let x = 1;
    x++;
    console.log(x);
}
y = 2;
console.log(y);
// console.log(x); // ReferenceError: x is not defined
// IF
const mayorEdad = 18;
// let edad = prompt('Dime tu edad');
let edad = 17;

if (edad >= mayorEdad) {
    console.log('eres mayor de edad');
    // document.writeln('eres mayor de edad');
} else {
    console.log('eres menor de edad');
    // document.writeln('eres menor de edad');
}

let semaforo = 'verde';
// SWITCH
switch (semaforo) {
    case 'rojo':
        console.log('Alto');
    break;
    case 'amarillo':
        console.log('Preventivo');
    break;
    case 'verde':
        console.log('Avanza');
    break;
    default: 
        console.log('opción no valida');
    break;   
}

// Do While
let x = 10;
do {
    console.log(x);    
    --x;
} while(x > 0);

// WHILE
x = 0;
while (x <= 10) {
    console.log(x); 
    x++;
}

let meses = [,'enero','febrero','marzo','abril', 'mayo', 'junio', 'julio', 'agosto', 'septiembre', 'octubre','noviembre', 'diciembre'];

// for
for (let i = 1; i <= 12; i++) {
    console.log(meses[i]);
}

// for in
for(let mes in meses) {
    console.log(mes);
}

// for of
for(let mes of meses) {
    console.log(mes);
}




