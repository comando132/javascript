let lenguajes = ['C', 'Java', 'PHP', 'Python'];
console.log(lenguajes);

// Añadir un elemento al final de un Array
lenguajes.push('JavaScript');
console.log(lenguajes);
// Eliminar el último elemento de un Arr                         ay
// lenguajes.pop();
// console.log(lenguajes);

// Añadir un elemento al principio de un Array
lenguajes.unshift('ASP');
console.log(lenguajes);

// Eliminar el primer elemento de un Array
// lenguajes.shift();
// console.log(lenguajes);

// Encontrar el índice de un elemento del Array
// console.log(lenguajes.indexOf('PHP'));

// Extrae una porción del array sobre el que se llama y devuelve un nuevo array.
// console.log(lenguajes.slice(0,3));

// Elimina una porción del array sobre el que se llama y devuelve un nuevo array.
// console.log(lenguajes);
// let eliminados = lenguajes.splice(1, 1);

// ordenar un arreglo 
lenguajes = ['c', 'Python', 'Java', 'GO', 'ASP', 'PHP', 'JavaScript'];
// lenguajes.sort();
// console.log(lenguajes);

// Une todos los elementos de una matriz en una cadena y devuelve esta cadena.
// console.log(lenguajes.join(', '));
// console.log(lenguajes.length);

// recorrer un arreglo 
/* lenguajes.forEach(function(dato, indice) {
    console.log(indice, dato);
}); */

// El método map() crea un nuevo array con los resultados de la llamada a la función indicada aplicados a cada uno de sus elementos.
// let lenguajesUpper = lenguajes.map((dato, index) => {
//     return index + ' => ' + dato.toUpperCase();
// });

// console.log(lenguajes);
// console.log(lenguajesUpper);

let cadena = 'Hola mundo';
console.log(cadena.replaceAll(' ','_'));
