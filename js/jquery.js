document.addEventListener("DOMContentLoaded", function() {
    $(document).ready(() => {
        //objeto jquery
        //seleccionar por id de elemento
        let id = $('#header');
        //seleccionar por clase de elemento
        let clase = $('.col');
        //seleccionar por elemento o tag
        let ele = $('div');
        // elimina el elemento del dom
        // $('.col').remove()
        //clase.remove()
        // selecionar todos los li hijos de un ul con la clase topnav
        // $( "ul.topnav > li" ).css( "background-color", "blue" );


        console.log(id, clase, ele);

        // agregar a un elemento
        clase.append("<b>ElEMENTO append</b>")
        clase.prepend("<b>ElEMENTO prepend</b>")
        clase.before("<p>ElEMENTO before</p>")
        clase.after("<p>ElEMENTO after</p>")
        id.addClass('d-none');
        id.text('Apareci')
        id.removeClass('d-none');



        //id.text('<h1 class="display-2">Display 2</h1>')

        $("li.bg-info").toggleClass("text-uppercase");

        $('ul').append('<li>un nuevo elemneto</li>')


    });
});