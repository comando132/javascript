// asignacion 
let x = 0;
console.log(x);
// asignacion con operacion
x += 1;
console.log(x);
// comparación
x = 0;
let y = false;
console.log(x);
console.log(y);

console.log(x == y, x === y);
console.log(x != y, x !== y);


x = 9;
y = 2;
console.log(x, y);
console.log(x > y, x < y);

// aritmeticos
console.log(x + y);
console.log(x - y);
console.log(x * y);
console.log(x / y);
console.log(x % y);

console.log(x, y);
console.log(x++);
console.log(x);
console.log(x--);
console.log(x);

console.log(++y);
console.log(y);
console.log(--y);
console.log(y);

// logicos
console.log(true && true, true && false, false && true , false && false);
console.log(true || true, true || false, false || true , false || false);
console.log(!true, !false);

//bit a bit
console.log(5 & 1);
console.log(5 | 1);
console.log(~5);
console.log(5 ^ 1);
console.log(5 << 1);
console.log(5 >> 1);
console.log(5 >>> 1);

//cadenas
let cad = 'hola'
console.log( cad + ' Mundo');
let txt = cad + ' ' + 'mundo';
console.log(txt);
console.log(5 + 5);
console.log('S' + 5);
console.log(5 + 'S');

let cadB = `${txt} aqui estoy`;
console.log(cadB);

edad = 17;
txtEdad = (edad >= 18) ? "adulto" : "menor";
console.log(txtEdad);
//console.log((edad >= 18) ? "adulto" : "menor" );

console.log(typeof txtEdad);
console.log(typeof edad);