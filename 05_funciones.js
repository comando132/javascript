// funcion que no regresa nada
function myFunction() {
    console.log('imprimo desde myfunction');
}

// funcion que regresa una cadena
function mi_funcion() {
    return "cadena de mi_function";
}

// funcion con argumentos
function aumentar2(num) {
    // num = parseInt(num);
    if (typeof num == 'number') { 
        return num + 2;
    } else {
        return 0;
    }

}

function concatenar() {
    console.log(typeof(arguments));
    console.log(arguments);
    let args = Array.prototype.slice.call(arguments);
    console.log(args);
}

function sumar(...args) {
    console.log(typeof(args));
    console.log(args);
}

/*
myFunction();
console.log(mi_funcion());
console.log(aumentar2(2));

concatenar('hola','como','estas');
*/
sumar(1,2,3,4)


function func(num = 0, num1 = 1, num2 = 2) {
    console.log(num);
    console.log(num1);
    console.log(num2);
}

func();


