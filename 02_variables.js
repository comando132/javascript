// null y undefined
let a = null;
let b;
console.log(a, b);

// number
let numero = 3;
let numero1 = 3.14;
let numero2 = -3.1E+12;
console.log(numero, numero1, numero2);

// binario octal o hexadecimal
let binario = 0b11101001010101010101;
let octal = 0o777;
let hexadecimal = 0xFF00FF;

console.log(binario, octal, hexadecimal);

// bigInt
let bigInt = 9007199254740992;
console.log(bigInt);

// bool
let bool = true;
console.log(bool);

// strings
let cadena = 'cadena';
let cadena1 = "cadena1";
let cadena2 = `otra cadena mas`;
console.log(cadena, cadena1, cadena2);

// arrays 
let dias = ["Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo"];
console.log(dias);

// función
let func = function() { return "hola"; }
console.log(func ,func());

// objeto
let objeto = {nombre: 'Hugo', apellido : 'Cuéllar'};
console.log(objeto);

// symbol
let sym = Symbol('foo');
console.log(sym);
