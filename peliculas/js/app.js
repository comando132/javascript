window.addEventListener('DOMContentLoaded', (event) => {
    function setCard(img, img_alt, title, text, url) {
        // operador ternario
        let img_url = (img == null) ? './img/no-image.svg' : `https://image.tmdb.org/t/p/w500${img}`;
        if (text == '') {
            text = 'No hay descripcion'
        }
        let card = `<div class="col">
            <div class="card h-100">
                <img src="${img_url}" class="card-img-top" alt="${img_alt}" title="${img_alt}">
                <div class="card-body">
                    <h5 class="card-title">${title}</h5>
                    <p class="card-text">${text.substring(0, 100)}</p>
                </div>
                <div class="card-footer">
                    <small class="text-muted">${url}</small>
                </div>
            </div>
         </div>`
         return card;
    }

    $(document).ready(() => {
        const queryString = window.location.search;
        const urlParams = new URLSearchParams(queryString);
        page = urlParams.get('page') ?? '1'
        const api = 'your_api_key'
        let url = 'https://api.themoviedb.org/3/movie/now_playing?region=MX&language=es-MX&api_key=[api]&page=[page]'
        // sustituimos el api_key y la url por sus valores
        url = url.replace('[api]', api);
        url = url.replace('[page]', page);
        $.ajax({
            type: "get",
            url: url,
            dataType: "json",
            success: (res) => {
                if (res.total_results && res.total_results > 0) {
                    $('#lista_peliculas').html('');
                    for (const pel of res.results) {
                        // una cadena html
                        let cardHtml = setCard(pel.poster_path, pel.original_title, pel.title, pel.overview)
                        $('#lista_peliculas').append(cardHtml);
                    }
                    if (res.total_pages > 1) {
                        $('#pagination').html('');
                        for (x=1; x <= res.total_pages; x++) {
                            let active = (x == res.page) ? 'active' : ''
                            $('#pagination').append(`<li class="page-item ${active}"><a class="page-link" href="javascript:void(0)">${x}</a></li>`);
                        }
                    }

                }
            }
        });

        //buscar peliculas
        //$(element).on('evento', funcion a ejecutar) == addEventListener()
        $('#btn_buscar').on('click', (e) => {
            let text = $('#inputBuscar');
            let url = 'https://api.themoviedb.org/3/search/movie?language=es-MX&page=1&region=MX&api_key=[API]&query=[TEXTO]';
            // sustituimos el api_key y la url por sus valores
            url = url.replace('[API]', api);
            url = url.replace('[TEXTO]', text.val());
            $.ajax({
                type: "GET",
                url: url,
                dataType: "json",
                success: (res) => {
                    if (res.total_results && res.total_results > 0) {
                        $('#lista_peliculas').html('');
                        for (const pel of res.results) {
                            // una cadena html
                            let cardHtml = setCard(pel.poster_path, pel.original_title, pel.title, pel.overview)
                            $('#lista_peliculas').append(cardHtml);
                        }
                    } else {
                        $('#lista_peliculas').html('<p class="p-3 mb-2 bg-info text-dark">No encontramos tu pelicula</p>');
                    }
                }
            });
        });
        // dar enter en el textod e busqueda
        $('#inputBuscar').on('keypress', (e) => {
            if (e.keyCode == 13) {
                let text = $('#inputBuscar');
                let url = 'https://api.themoviedb.org/3/search/movie?language=es-MX&page=1&region=MX&api_key=[API]&query=[TEXTO]';
                // sustituimos el api_key y la url por sus valores
                url = url.replace('[API]', api);
                url = url.replace('[TEXTO]', text.val());
                $.ajax({
                    type: "GET",
                    url: url,
                    dataType: "json",
                    success: (res) => {
                        if (res.total_results && res.total_results > 0) {
                            $('#lista_peliculas').html('');
                            for (const pel of res.results) {
                                // una cadena html
                                let cardHtml = setCard(pel.poster_path, pel.original_title, pel.title, pel.overview)
                                $('#lista_peliculas').append(cardHtml);
                            }
                        } else {
                            $('#lista_peliculas').html('<p class="p-3 mb-2 bg-info text-dark">No encontramos tu pelicula</p>');
                        }
                    }
                });
            }
        });

        $("#pagination").on( "click", "li", function() {
            let page = $(this).text();
            let url = 'https://api.themoviedb.org/3/movie/now_playing?region=MX&language=es-MX&api_key=[api]&page=[page]'
            // sustituimos el api_key y la url por sus valores
            url = url.replace('[api]', api);
            url = url.replace('[page]', page);
            $.ajax({
                type: "get",
                url: url,
                dataType: "json",
                success: (res) => {
                    if (res.total_results && res.total_results > 0) {
                        $('#lista_peliculas').html('');
                        for (const pel of res.results) {
                            // una cadena html
                            let cardHtml = setCard(pel.poster_path, pel.original_title, pel.title, pel.overview)
                            $('#lista_peliculas').append(cardHtml);
                        }
                        if (res.total_pages > 1) {
                            $('#pagination').html('');
                            for (x=1; x <= res.total_pages; x++) {
                                let active = (x == res.page) ? 'active' : ''
                                $('#pagination').append(`<li class="page-item ${active}"><a class="page-link" href="javascript:void(0)">${x}</a></li>`);
                            }
                        }

                    }
                }
            });
        });
    });
});