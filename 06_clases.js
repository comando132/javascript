class Persona {
    constructor (nombre, ap1, ap2) {
        this.nombre = nombre;
        this.ap1 = ap1;
        this.ap2 = ap2;
    }
    
    // sobreescribir el metodo toString
    toString () {
        return `${this.nombre} ${this.ap1} ${this.ap2}`;
    } 
}

class Usuario extends Persona {
    constructor(nombre, ap1, ap2, usuario) {
        super(nombre, ap1, ap2);
        this.usuario = usuario;
    }

    logueo() {
        console.log(this.nombre, this.usuario);
    }
}

let persona = new Persona('Hugo', 'Cuellar', 'Martinez');

let usuario = new Usuario('Hugo', 'Cuellar', 'Martinez', 'hugoc');

persona.nombre = "aAAA";

console.log(persona.toString());

usuario.logueo();
